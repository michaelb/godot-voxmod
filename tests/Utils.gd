extends "./unittest.gd"

const Utils = preload("../Utils.gd")

func tests():
    var basedir = self.get_script().get_path().get_base_dir()

    testcase('Recursively lists directory')
    var path = basedir.plus_file('data/testmod1')
    var results = Utils.recursive_files_list(path)
    assert_true(results.has(path.plus_file('entity_templates.cfg')), 'found top level file')
    assert_true(results.has(path.plus_file('maps/testmap.cfg')), 'found deeper file')
    endcase()

    testcase('Combines images into master texture')
    var test_textures = [
        basedir.plus_file('data/testmod1/materials/red_blue_tile_1.png'),
        basedir.plus_file('data/testmod1/materials/red_white_tile_2.png'),
        basedir.plus_file('data/testmod1/materials/green_gray_tile_3.png'),
    ]
    var combined = Utils.combine_images(16, test_textures)
    var exp_size = Rect2(0, 0, 16*3*3, 16*3)
    combined.save_png(basedir.plus_file('tmpoutput/combined_1.png'))
    assert_eq(combined.get_used_rect(), exp_size, 'resulting image has expected shape of opaque pixels')
    endcase()
