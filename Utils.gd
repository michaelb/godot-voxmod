const _recurse_protection = [0]

const EMPTY = 0 # TODO get from voxtools

static func warn(msg):
    print("- WARNING: " + msg)

static func _recursive_files_list(path, results, _reclvl):
    _reclvl += 1
    if _reclvl > 100:
        print("voxmod.Utils: Infinite directory recursion detected.")
        return

    var dir = Directory.new()
    if dir.open(path) != OK:
        print("voxmod.Utils: Can't access path: " + path)
        return

    dir.list_dir_begin()
    var file_name = dir.get_next()
    while file_name != "":
        var full_path = path.plus_file(file_name)
        if file_name.begins_with('.'):
            # print("ignoring ", file_name)
            pass
        elif dir.current_is_dir():
            # print("recursing ", full_path)
            _recursive_files_list(full_path, results, _reclvl)
        else:
            # print("appending ", full_path)
            results.append(full_path)
        file_name = dir.get_next()

static func recursive_files_list(path):
    #
    # Recursively list all files in given path (skips dirs, empty dirs)
    var results = []
    _recursive_files_list(path, results, 0)
    return results

static func combine_images(size, texture_list):
    # For now, it creates 9x every texture square to allow the pixels to
    # bleed without jagged edges creating quads.
    # NOTE: This is totally excessive, but works for now
    var count = texture_list.size()
    var master_image_size = Vector2()
    var master_image = Image(count * 3 * size, 3 * size, true, Image.FORMAT_RGBA)

    var image_i = 0
    for path in texture_list:
        # Loads each image
        var image = Image(size, size, false, Image.FORMAT_RGBA)
        image.load(path)
        for x_offset in range(0, 3):
            for y_offset in range(0, 3):
                # Blits each child image onto the master texture image
                var loc = Vector2((x_offset + image_i) * size, y_offset * size)
                var rect = Rect2(0, 0, size, size)
                master_image.blit_rect(image, rect, loc)
        image_i += 3
    return master_image

# TODO: move these to godot-voxtools

#
# Returns a list of 1s and 0s indicating which cube faces should be
# drawn, based on what is exposed
static func get_needed_cube_faces(material_vox3d, v):
    #    needed_faces is structured x (left,right) y (up,down) z (front, back) [0,0,1,0,0,0]
    var sides = [
        Vector3(1,  0,  0),
        Vector3(-1, 0,  0),
        Vector3(0,  1,  0),
        Vector3(0, -1,  0),
        Vector3(0,  0,  1),
        Vector3(0,  0, -1),
    ]
    var needed_cube_faces = [0, 0, 0, 0, 0, 0]
    var normals = [Vector3(1, 0, 0), Vector3(0, 1, 0), Vector3(0, 0, 1)]
    var negative_positive = [1, -1]
    for i in range(6):
        var val = material_vox3d.get_by_vector(v + sides[i])
        if val == EMPTY:
            needed_cube_faces[i] = 1

    # NOTE: never renders bottom or back face, also can be enhanced by
    # not rendering at the edge of TChunk (e.g. an outer wall)
    #if disable_bottom_and_back:
    #    needed_cube_faces[3] = 0
    #    needed_cube_faces[5] = 0
    return needed_cube_faces

#
# Returns true if its exposed on any side
static func is_block_exposed(vox3d, v):
    var sides = [
        Vector3(1,  0,  0),
        Vector3(0,  1,  0),
        Vector3(0,  0,  1),
        Vector3(-1, 0,  0),
        Vector3(0, -1,  0),
        Vector3(0,  0, -1),
    ]
    for side in sides:
        var val = vox3d.get_by_vector(v + side)
        if val == EMPTY:
            return true
    return false

