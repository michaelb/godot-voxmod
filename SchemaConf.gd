# Simple and ugly implementation of a subset of the SchemaConf format
# in GDScript
# It ONLY supports:
# [entry]
# field = value
# NO ":" assignments whatsoever
########################

var _current_entry_name
var _current_entry_data
var _current_field_name
var _current_field_data
# var _current_field_delim # TODO

var entry_name_regexp
var assignment_regexp
var comment_regexp
#var multiline_assignment_regexp
# var delim_regexp # TODO

func _init():
    entry_name_regexp = RegEx.new()
    assignment_regexp = RegEx.new()
    comment_regexp = RegEx.new()

    # print("Compiling entry name regexp")
    entry_name_regexp.compile("^\\s*\\[(\\w+)]\\s*$")
    # print("Compiling assignment regexp")
    assignment_regexp.compile("^\\s*(\\w+)\\s*=\\s*(.*)")
    # print("Compiling comment regexp")
    comment_regexp.compile("^\\s*#.*")

func _reset():
    _current_entry_name = null
    _current_entry_data = null
    _current_field_name = null
    _current_field_data = null
    # _current_field_delim = null

func _add_entry(data):
    if _current_entry_name == null:
        if _current_entry_data != null:
            print("Conf Syntax Error: Loose data in cfg file")
        return

    if not data.has(_current_entry_name ):
        data[_current_entry_name] = []
    data[_current_entry_name].append(_current_entry_data)
    _current_entry_name = null
    _current_entry_data = null

func _add_assignment(data):
    _current_entry_data[_current_field_name] = _current_field_data
    _current_field_name = null
    _current_field_data = null

func open(path):
    _reset()
    var data = {}
    var f = File.new()
    f.open(path, File.READ)
    if not f.is_open():
        print("SchemaConf.gd: Cannot open ", path)
        return

    while not f.eof_reached():
        var line = f.get_line()

        #### COMMENT
        if comment_regexp.find(line) != -1:
            continue

        #### ENTRY
        if entry_name_regexp.find(line) != -1:
            _add_entry(data)
            _current_entry_name = entry_name_regexp.get_capture(1)
            _current_entry_data = {}
            continue

        #### DATA
        if assignment_regexp.find(line) != -1:
            _current_field_name = assignment_regexp.get_capture(1)
            _current_field_data = assignment_regexp.get_capture(2)
            _add_assignment(data)
            continue

    f.close()
    _add_entry(data) # add final entry
    return data
