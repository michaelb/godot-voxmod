# World algorithm

const Simplex = preload('../lib/Simplex.gd')
var type
var entity_type = 'none'
var info
var pattern = null
var material = 1

# TODO should put into consts
const CHUNK_D = Vector3(64, 64, 64)

var noise_hill_height = 8
var noise_hill_density = 0.2

var world_names = null
var worlds = null

var never_gen_chunk = false
var subworld_default = false
var subworld_noise_height = 8
var subworld_noise_thresh = 6
var subworld_noise_density = 0.2

func setup(info):
    type = info['type']
    self.info = info
    #path = info['path'] # for 
    if type == 'tiled-pattern':
        pattern = info['pattern']
    if type == 'simplex-noise':
        if info.has('material'):
            material = int(info['material'])
        if info.has('height'):
            noise_hill_height = int(info['height'])
        if info.has('density'):
            noise_hill_density = float(info['density'])

    if type == 'multi-world':
        world_names = info['worlds'].split(',')
        entity_type = 'multi-world'

    if info.has('entity_type'):
        entity_type = info['entity_type']

    if info.has('is_subworld'):
        never_gen_chunk = true

    if info.has('subworld_default'):
        subworld_default = true

    elif info.has('subworld_noise_height'):
        subworld_noise_height = int(info['subworld_noise_height'])
        subworld_noise_thresh = float(info['subworld_noise_thresh'])
        subworld_noise_density = float(info['subworld_noise_density'])

func setup_modloader(modloader):
    # Set up sub-worlds if applicable
    if world_names == null:
        return

    worlds = []
    for world_name in world_names:
        var world = modloader.get('world', world_name)
        worlds.append(world)

#
# Returns "true" if this generator should handle generating this chunk.
# Note: For now, only 1 world gen allowed per WorldData chunk, so if it returns
# true, it should generate the entire chunk.
func check_chunk(chunk_vector):
    if never_gen_chunk:
        return false

    if type == 'simplex-noise':
        return true # It can always gen if its an infini-simplex noise pattern
    if type == 'multi-world':
        return true # It can always gen if its a "multiworld"
    if type == 'test-pillar-plane':
        return true # It can always gen if its a test pillar plane
    if type == 'tiled-pattern':
        return true # It can always gen if its an infini-tiled pattern
    return false

func _test_pillar_plane(voldata):
    printerr("World.gd: Generating test pillar plane")
    var max_x = voldata.max_x
    var max_z = voldata.max_z
    #for x in range(max_x):
    #    for z in range(max_z):
    #        var vector = Vector3(x, 0, z)
    #        var type = 1
    #        voldata.add_block(vector, type)

    # And add in some random brick blocks
    for x in range(max_x):
        for z in range(max_z):
            var height = Simplex.simplex2(x, z)
            for y in range(height):
                var type = material
                voldata.add_block(Vector3(x, y, z), type)
                voldata.add_block(Vector3(x, y, z), type)
    return voldata

func _simplex_noise(voldata, chunk_vector):
    var cv = chunk_vector * CHUNK_D # computer global offset
    # Only do at surface level
    if chunk_vector.y < 0:
        return voldata
    if chunk_vector.y > 16:
        return voldata

    var max_x = voldata.max_x
    var max_z = voldata.max_z
    for x in range(max_x):
        for z in range(max_z):
            self._render_column(voldata, cv, x, z)
            continue
    return voldata

#
# Given renders a vertical column according to this world generator
func _render_column(voldata, cv, x, z):
    # Add floor / plain
    var vector = Vector3(x, 0, z)
    voldata.add_block(vector, material)

    # Add in hills
    var x_s = (x + cv.x) * noise_hill_density
    var z_s = (z + cv.z) * noise_hill_density
    var float_height = Simplex.simplex2(x_s, z_s)
    if float_height < 0:
        return
    var int_height = int(float_height * noise_hill_height)
    for y in range(int_height):
        voldata.add_block(Vector3(x, y, z), material)
        voldata.add_block(Vector3(x, y, z), material)

#
# Adds the required entities to the given column
#func _render_column_entities(entity_manager, voldata, cv, x, z):
#    pass


#
# Generates a chunk using the "multi world" strategy, that is,
# utilizes subworld generates blending them based on column
func _multi_world(voldata, chunk_vector):
    var cv = chunk_vector * CHUNK_D # computer global offset
    # Only do at surface level
    if chunk_vector.y < 0:
        return voldata
    if chunk_vector.y > 16:
        return voldata

    var max_x = voldata.max_x
    var max_z = voldata.max_z
    for x in range(max_x):
        for z in range(max_z):
            #var vector = Vector3(x, 0, z)
            #var type = 1
            #voldata.add_block(vector, type)
            var global_x = (x + cv.x)
            var global_z = (z + cv.z)

            var subworld = _get_subworld(global_x, global_z)

            if subworld == null:
                printerr('No subworld for column ' + Vector2(global_x, global_z))
                continue

            subworld._render_column(voldata, cv, x, z)

    return voldata

#
# Generates entities for hte mutli-world strategy
#func _multi_world_entities(entity_manager, voldata, chunk_vector):
#    var cv = chunk_vector * CHUNK_D # computer global offset
#    # Only do at surface level
#    if chunk_vector.y < 0 or chunk_vector.y > 16:
#        return
#
#    for x in range(voldata.max_x):
#        for z in range(voldata.max_z):
#            var global_x = (x + cv.x)
#            var global_z = (z + cv.z)
#
#            var subworld = _get_subworld(global_x, global_z)
#
#            if subworld == null:
#                printerr('No subworld for column ' + Vector2(global_x, global_z))
#                continue
#
#            subworld._render_column_entities(entity_manager, voldata, cv, x, z)


#
# Returns the appropriate subworld for the given location
func _get_subworld(global_x, global_z):
    for subworld in worlds:
        if subworld._check_subworld_column(global_x, global_z):
            # It found a subworld that wants to cover this column
            return subworld
    return null

#
# Used for biomes: Determine which subworld should be at this column
func _check_subworld_column(global_x, global_z):
    if subworld_default:
        return true

    # Employ simplex algo for determining "blotches" of subworlds
    var x_s = global_x * subworld_noise_density
    var z_s = global_z * subworld_noise_density
    var float_height = Simplex.simplex2(x_s, z_s)
    var int_height = int(float_height * subworld_noise_height)
    if int_height > subworld_noise_thresh:
        return true
    return false

func _tiled_pattern(voldata, chunk_vector):
    # TODO: have it offset based on chunk_vector
    #pattern.gen_chunk(voldata, chunk_vector)
    voldata.material_vox3d = pattern.material
    voldata.shape_vox3d = pattern.terrain

func gen_chunk(voldata, chunk_vector):
    # For now, just gen plane
    if type == 'test-pillar-plane':
        _test_pillar_plane(voldata)
    elif type == 'tiled-pattern':
        _tiled_pattern(voldata, chunk_vector)
    elif type == 'simplex-noise':
        _simplex_noise(voldata, chunk_vector)
    elif type == 'multi-world':
        _multi_world(voldata, chunk_vector)

#func gen_entities(entity_manager, voldata, chunk_vector):
#    if entity_type == 'multi-world':
#        _multi_world_entities(entity_manager, voldata, chunk_vector)
#    elif entity_type == 'random':
#        pass
#        # _random_entities(entity_manager, voldata, chunk_vector)

func spawn_callbacks(callback):
    if type == 'multi-world':
        pass #_multi_world_spawn_callbacks(chunk_v, spawn_callbacks)




# Generates callbacks for hte mutli-world strategy
func _multi_world_entities(entity_manager, voldata, chunk_vector):
   var cv = chunk_vector * CHUNK_D # computer global offset
   if chunk_vector.y < 0 or chunk_vector.y > 16:
       return # Only do at surface level

   for x in range(voldata.max_x):
       for z in range(voldata.max_z):
           var global_x = (x + cv.x)
           var global_z = (z + cv.z)

           var subworld = _get_subworld(global_x, global_z)

           if subworld == null:
               printerr('No subworld for column ' + Vector2(global_x, global_z))
               continue

           subworld._do_column_callbacks()

