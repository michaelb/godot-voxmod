const SchemaConf = preload('./SchemaConf.gd')
const Dict3D = preload('../godot-voxtools/Dict3D.gd')
const Dict3DConsts = preload('../godot-voxtools/Consts.gd')

var path

var dict3d = null
var _blocks = null
var conf_data = {}
var tiles_by_id = {}
var entity_manager = null

var width
var height
var depth
var array_size
var EMPTY = Dict3DConsts.EMPTY

func _init(path):
    self.path = path
    self.load_path()

func load_path():
    # FIRST GET JSON
    var conf = SchemaConf.new()
    conf_data = conf.open(path)

    # Now set "height" "width" and "depth"
    var vector = get_world_dimension_vector()
    width = vector.x
    height = vector.y
    depth = vector.z

    # Set up volume dict
    dict3d = Dict3D.new(width, height, depth)
    # Make out of bounds solid, and warn when used
    #dict3d.set_out_of_bounds(Dict3D.CUBE, true) # More correct
    dict3d.set_out_of_bounds(Dict3D.EMPTY, false)

    # And set up a dict keying tiles by ID
    for tile in conf_data['tile']:
        tiles_by_id[int(tile['id'])] = tile

    # Set defaults
    if not conf_data.has('entity_template'):
        conf_data['entity_template'] = []
    if not conf_data.has('entity'):
        conf_data['entity'] = []

    # Now set up an entity manager
    #entity_manager = EntityManager.new(conf_data['entity_template'], conf_data['entity'])

    # And read in the chunk blocks
    var chunk_data_fn = conf_data['data'][0]['path']
    var base_path = path.get_base_dir()
    var chunk_data_path = base_path.plus_file(chunk_data_fn)
    var f
    f = File.new()
    var err = f.open(chunk_data_path, File.READ)
    if not f.is_open():
        print("OH NO, cannot open ", chunk_data_path)
    else:
        _blocks = RawArray()
        array_size = width * height * depth
        _blocks = f.get_buffer(array_size)
        dict3d.copy_from(_blocks) # copy from the other raw array
        f.close()

func save_world_data():
    print("NOT IMPLEMENTED YET")
    return
    # And read in the chunk blocks
    var chunk_data_path = self.path + '.chunk_data'
    var f
    f = File.new()
    var err = f.open(chunk_data_path, File.WRITE)
    if not f.is_open():
        print("OH NO, cannot open ", chunk_data_path)
        return
    f.store_buffer(_blocks)
    f.close()

#func get_voxel_chunk_data():
#    return blocks

func get_world_dimension_vector():
    var d = conf_data['data'][0]
    return Vector3(int(d['max_x']), int(d['max_y']), int(d['max_z']))

const BOUNDARY = 1
func is_out_of_bounds(vec):
    # TODO use built-in math
    # NOTE: uses 1 block boundary. This is to prevent Physics.gd from throwing
    # an error when the moment propels a bullet past the boundary. In reality,
    # this should be checked for in physics.
    var d = get_world_dimension_vector() - Vector3(BOUNDARY, BOUNDARY, BOUNDARY)
    return vec.x < BOUNDARY or vec.y < BOUNDARY or vec.z < BOUNDARY or vec.x >= d.x or vec.y >= d.y or vec.z >= d.z

func get_index(vector):
    #return int((vector.x * depth * height) + (vector.y * depth) + vector.z)
    return int(vector.x + (vector.z * width) + (vector.y * depth * width))

func get_vector(index):
    pass # TODO: need to figure out this math

func get_block(vector):
    return dict3d.get_by_vector(vector)

#########################
# After loaded, add an additional block
func add_block(vector, type):
    dict3d.init_block(vector.x, vector.y, vector.z, type)

func remove_block(vector):
    dict3d.init_block(vector.x, vector.y, vector.z, dict3d.EMPTY)

