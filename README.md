NOTE: very WIP, nothing really to see here!

# godot-voxmod

Mod system for a particular flavor of voxel-type games.

This is generally just made for my own games, but I'm releasing it
freely so it can be used for other folks if their uses happen to align
with mine! That said, keeping documentation at top quality is not a
high priority for me.

## requirements

This depends on another of my modules, `godot-voxtools`.

It assumes that `godot-voxtools` is available in a directory up one in
the dir hierarchy from this library.

E.g., the dir structure I use:

    .git/
    game/
        engine.ini
        scripts/
        scenes/
        submodules/
            godot-voxmod/
            godot-voxtools/


For running tests, soft-linking requirements in root directory ends up
working fine.

## conceptual structure

This is how data is arranged

- mod: Top level. Provides...
    - Materials (block types) (was "tiles")
    - Entity Templates
    - Items
    - VoxMap (serialized world or chunk, or game stage)
        - Volume data
            - Block-material data (using IDs found in Materials)
            - Block-shape data (Dict3D encoded)
        - Entity instances (extending templates)

Everything below mod has a "name".

##  mod dir structure

Presently, there is no fixed dir structure for mods. Anything can be anywhere.
It will recurse and ingest all .cfg files.


##  custom types

Custom types can be created with [datatype], as follows:

```
[datatype]
name = item
inventory_image = image
```

This will cause the `inventory_image` field to actually load the given path as
an image.

Eventually everything will switch to this, and built-ins will be the same
format but just built-in.


