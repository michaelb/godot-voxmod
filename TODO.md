## Entity storage notes

* Entity storage SHOULD NOT use schema conf

* Schema conf should be read only, just for human-readable format to build mods
  and game data

* Use JSON or something similar for entity placement (same as save-game data
  format)

## Still need to do

* Generalize inheritance, possibly replacing "template" concept

* Solidify / formalize mod concept, should have a "voxmod.cfg" at root, and
  Utils.gd should have functions to walk up and find containing mod

## WorldVoxData

For infinite-ish voxel worlds. Should have identical interface to
Dict3D for checking, but also provides "meta-level" management of
loading/unloading chunks. This maybe should be implemented in
godot-voxmod package?

Interface as follows:

* `signal load_chunk(x, y, z)` -- A signal for populating a given
    chunk, that should be connected to the world gen / loading algo
* `signal unload_chunk(x, y, z)` -- inverse of above, should do a "diff" to
    see if dirty, and if dirty, serialize to disk
* `set_center(x, y, z)` -- Move simulation center. This in turn triggers
    `load_chunk` and `unload_chunk`
* `set_chunk_radius(r)` -- Sets a chunk radius, e.g. autloads within `r`
    chunks around center

Also, implement the interface from VolData, as such:
* `world_data.get_block(vector)`
* `world_data.EMPTY`
* `world_data.is_block_exposed(vector)`
* `world_data.get_needed_cube_faces(vector)`
* `world_data.trace_ray_collision(origin, ray)`
* `world_data.set_block(vector, material)`
* `world_data.delete_block(vector)`
* `world_data.get_world_dimension_vector()` - total size of world

### Simulation management

If data outside of simulation is requested, it should actually *pause* the
simulation, triggering a `load_chunk`, and only unpause when the chunk is
loaded (for multi-threaded applications). If `chunk_radius` is big enough, then
this should never happen.


#### Paused chunking idea

Another idea: Simulation should be "full" in r-radius chunks, but "paused" in
r+1 radius chunks. That is to say, r+1 gets loaded, and is active, but anything
that enters these chunks gets "frozen" (paused). Thus, if for example a agent
(trade caravan, lets say) begins traveling in a given direction, it will just
get "stuck" when it gets to r+1, and eventually unloaded / serialized if the
player moves away, OR the opposite, if the player moves in that direction, the
agent's AI is unpaused again, and it continues on its merry way.

Potential pitfall: "build-up" of stuck things at r+1. E.g., imagine a magic
bolt that continues forever. When it hits r+1, it will just get stuck on the
edge, until that is loaded, then many could all start at once.  A way to
address this is to add rules for paused chunking, such as expiry-time for all
agents that are pausable. Thus, magic bolts expire very quickly, while trade
caravans not as quickly.


#### DONE:

* "Schema-defined" generalized object system that can be added (would make
  editor later easier)
    * For example, objects would be "Item", "Entity Template", and "Mob"
